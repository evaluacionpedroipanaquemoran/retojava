package com.example.retoJava.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import com.example.retoJava.models.UserData;
import com.example.retoJava.models.UserResponse;
import com.example.retoJava.service.impl.UserServiceImpl;

public class UserServiceTest {

	@Mock
	private RestTemplate restTemplate;

	private UserServiceImpl userService;

	private UserResponse mockUserResponse;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);

		// Datos de prueba para mockUserResponse
		UserData userData1 = new UserData(1, "Doe", "john.doe@example.com");
		UserData userData2 = new UserData(2, "Smith", "jane.smith@example.com");
		List<UserData> userList = Arrays.asList(userData1, userData2);
		mockUserResponse = new UserResponse();
		mockUserResponse.setData(userList);

		// Configurar el comportamiento del mock restTemplate
		String apiURL = "https://reqres.in/api/users";
		when(restTemplate.getForObject(apiURL, UserResponse.class)).thenReturn(mockUserResponse);

		// Crear manualmente una instancia de UserServiceImpl y pasar el restTemplate
		// mockeado
		userService = new UserServiceImpl(apiURL, restTemplate);
	}

	@Test
	public void testCreateUser() {
		// Ejecutar el método que estamos probando
		List<UserData> result = userService.createUser();

		// Verificar los resultados sin Hamcrest
		assertEquals(2, result.size());

		assertEquals(1, result.get(0).getId());
		assertEquals("Doe", result.get(0).getLast_name());
		assertEquals("john.doe@example.com", result.get(0).getEmail());

		assertEquals(2, result.get(1).getId());
		assertEquals("Smith", result.get(1).getLast_name());
		assertEquals("jane.smith@example.com", result.get(1).getEmail());
	}
}
