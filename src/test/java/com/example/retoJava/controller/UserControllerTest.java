package com.example.retoJava.controller;

import com.example.retoJava.models.UserData;
import com.example.retoJava.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
public class UserControllerTest {
	@Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void testReestructurarRespuesta() throws Exception {
        // Datos de prueba
        UserData userData1 = new UserData(1, "Bluth", "george.bluth@reqres.in");
        UserData userData2 = new UserData(2, "Weaver", "janet.weaver@reqres.in");
        List<UserData> userList = Arrays.asList(userData1, userData2);

        // Configurar el comportamiento del mock userService
        when(userService.createUser()).thenReturn(userList);

        // Realizar la solicitud POST y verificar los resultados
        mockMvc.perform(post("/reestructurar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].last_name").value("Bluth"))
                .andExpect(jsonPath("$[0].email").value("george.bluth@reqres.in"))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].last_name").value("Weaver"))
                .andExpect(jsonPath("$[1].email").value("janet.weaver@reqres.in"));
    }

}
