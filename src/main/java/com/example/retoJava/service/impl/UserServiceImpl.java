package com.example.retoJava.service.impl;

import com.example.retoJava.models.UserData;
import com.example.retoJava.models.UserResponse;
import com.example.retoJava.service.UserService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

	private final String apiURL;
	private final RestTemplate restTemplate;

	public UserServiceImpl(@Value("${apiURL}") String apiURL, RestTemplate restTemplate) {
		this.apiURL = apiURL;
		this.restTemplate = restTemplate;
	}

	@Override
	public List<UserData> createUser() {
		UserResponse response = restTemplate.getForObject(apiURL, UserResponse.class);

		return response.getData().stream().map(user -> new UserData(user.getId(), user.getLast_name(), user.getEmail()))
				.collect(Collectors.toList());
	}
}
