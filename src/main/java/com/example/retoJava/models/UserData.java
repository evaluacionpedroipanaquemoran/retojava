package com.example.retoJava.models;

import java.util.Objects;

public class UserData {

	private int id;
	private String last_name;
	private String email;

	public UserData() {
	}

	public UserData(int id, String last_name, String email) {
		this.id = id;
		this.last_name = last_name;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	// Implementación del método equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserData)) return false;
        UserData userData = (UserData) o;
        return id == userData.id &&
                Objects.equals(last_name, userData.last_name) &&
                Objects.equals(email, userData.email);
    }

    // Implementación del método hashCode
    @Override
    public int hashCode() {
        return Objects.hash(id, last_name, email);
    }

}
