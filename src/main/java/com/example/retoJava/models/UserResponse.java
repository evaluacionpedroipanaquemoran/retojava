package com.example.retoJava.models;

import java.util.List;

public class UserResponse {

	private List<UserData> data;

	public List<UserData> getData() {
		return data;
	}

	public void setData(List<UserData> data) {
		this.data = data;
	}
	
	

}
