package com.example.retoJava.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.retoJava.models.UserData;
import com.example.retoJava.service.UserService;

@RestController
public class UserController {

	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping("/reestructurar")
	public List<UserData> reestructurarRespuesta() {
		return userService.createUser();
	}
}
